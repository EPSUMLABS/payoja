<?php

require_once 'vendor/autoload.php';

// Create the Transport
$transport = (new Swift_SmtpTransport('mail.payoja.co', 465))
  ->setUsername('support@payoja.co')
  ->setPassword('asdfg12345ASDFG!@#$%')
  ->setStreamOptions(array('tls' => array('allow_self_signed' => true, 'verify_peer' => false)));
;

// Create the Mailer using your created Transport
$mailer = new Swift_Mailer($transport);

// Create a message
$message = (new Swift_Message('Wonderful Subject'))
  ->setFrom(['support@payoja.co' => 'Payoja'])
  ->setTo(['kajorib9@gmail.com' => 'A name'])
  ->setBody('Here is the message itself')
  ;

// Send the message
$result = $mailer->send($message);
echo $result;

?>