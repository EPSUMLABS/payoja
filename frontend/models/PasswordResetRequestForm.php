<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\helpers\Html;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    public function send_mail($to,$name,$subject,$body,$altbody){
        $mail = new PHPMailer(true);                             // Passing `true` enables exceptions
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'payoja.co@gmail.com';                 // SMTP username
		$mail->Password = 'Poonam@panda1';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to

		//Recipients
		$mail->setFrom('payoja.co@gmail.com', 'Payoja');
		$mail->addAddress($to, $name);     // Add a recipient


		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = $subject;
		$mail->Body    = $body;
		$mail->AltBody = $altbody;

		$mail->send();
		return true;
	} catch (Exception $e) {
		echo false;
	}

}
    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }
        $mailer = Yii::$app->mailer;
        $message = $mailer->compose(
            ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
            ['user' => $user]
        )
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . 'Payoja Admin'])
        ->setTo($this->email)
        ->setSubject('Password reset for Admin')
        ->send();

        if ($message === true) {
            return $message;
        } else {
            echo 'Error!<br>';
            echo '<pre>' . print_r($mailer->getErrors(), true) . '</pre>';
        }

        // $to=$this->email;
        // $name=$user['username'];
        // $subject='Reset Password';
        // $from='supprt@payoja.co';
        // $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
        
        // $altbody='<h3>Dear '.$name.'</h3><br>'.'<h4>Follow this link to Reset your Password</h4><br>'.Html::a(Html::encode($resetLink), $resetLink);
        // $result= $this->send_mail($to,$name,$subject,$altbody,$from);
        // return $result;
        // return Yii::$app
        //     ->mailer
        //     ->compose(
        //         ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
        //         ['user' => $user]
        //     )
        //     ->setFrom([Yii::$app->params['supportEmail'] =>'Payoja Admin'])
        //     ->setTo($this->email)
        //     ->setSubject('Password reset for Admin')
        //     ->send();
    }
}
