<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ResetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/animate.css',
        'css/bootstrap.css',
        'css/waves.css',
        'css/style.css',
        'css/materialize.css'
    ];
    public $js = [
        'js/admin.js',
        'js/bootstrap.js',
        'js/jquery.min.js',
        'js/jquery.validate.js',
        'js/sign-in.js',
        'js/waves.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
