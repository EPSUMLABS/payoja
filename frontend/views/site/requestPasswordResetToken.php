<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
<div class="row">
<div class="col-lg-4">
<div class="card">
<div class='header'>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out your email. A link to reset password will be sent there.</p>
    </div>
        <div class='body'>

   
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

               <center> <div class="form-group">
                    <?= Html::submitButton('SUBMIT', ['class' => 'btn btn-primary', 'name' => 'login-button','style'=>'width:85%;height:35px']) ?>
                </div></center>
                <div style="color:#999;margin:1em 0">
                   Back To&nbsp;&nbsp;<?= Html::a('Login', ['../../../backend/web/index.php/site/login']) ?>.
                </div>

            <?php ActiveForm::end(); ?>
            </div>
            </div>
        </div>
    </div>
</div>