<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-reset-password">
<div class="row">
<div class="col-lg-4">
<div class="card">
<div class='header'>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please choose your new password:</p>

  </div>
        <div class='body'>

            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

               <center>  <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'login-button','style'=>'width:85%;height:35px']) ?>
                </div></center>
                <div style="color:#999;margin:1em 0">
                   Back To&nbsp;&nbsp;<?= Html::a('Login', ['../../../backend/web/index.php/site/login']) ?>.
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            </div>
        </div>
    </div>
</div>