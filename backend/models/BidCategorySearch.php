<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BidCategory;

/**
 * BidCategorySearch represents the model behind the search form of `common\models\BidCategory`.
 */
class BidCategorySearch extends BidCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bidcat_id'], 'integer'],
            [['bidcat_name', 'bidcat_addedon'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BidCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bidcat_id' => $this->bidcat_id,
            'bidcat_addedon' => $this->bidcat_addedon,
        ]);

        $query->andFilterWhere(['like', 'bidcat_name', $this->bidcat_name]);

        return $dataProvider;
    }
}
