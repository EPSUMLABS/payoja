<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserPayout;

/**
 * UserPayoutSearch represents the model behind the search form of `common\models\UserPayout`.
 */
class UserPayoutSearch extends UserPayout
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payout_id', 'user_id', 'paytm_phoneno'], 'integer'],
            [['user_name', 'user_bankacc_no', 'user_bankifsc_code', 'transaction_id', 'status', 'date_of_request'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPayout::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'payout_id' => $this->payout_id,
            'user_id' => $this->user_id,
            'paytm_phoneno' => $this->paytm_phoneno,
            'amount' => $this->amount,
            'date_of_request' => $this->date_of_request,
        ]);

        $query->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'user_bankacc_no', $this->user_bankacc_no])
            ->andFilterWhere(['like', 'user_bankifsc_code', $this->user_bankifsc_code])
            ->andFilterWhere(['like', 'transaction_id', $this->transaction_id])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
