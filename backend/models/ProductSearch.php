<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form of `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pid', 'p_soldto', 'p_isdelete'], 'integer'],
            [['p_name', 'p_price', 'p_bid_price', 'p_start_time', 'p_bidcategory', 'p_lastbid', 'p_imgpath', 'p_addedon'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pid' => $this->pid,
            'p_soldto' => $this->p_soldto,
            'p_lastbid' => $this->p_lastbid,
            'p_addedon' => $this->p_addedon,
            'p_isdelete' => $this->p_isdelete,
        ]);

        $query->andFilterWhere(['like', 'p_name', $this->p_name])
            ->andFilterWhere(['like', 'p_price', $this->p_price])
            ->andFilterWhere(['like', 'p_bid_price', $this->p_bid_price])
            ->andFilterWhere(['like', 'p_start_time', $this->p_start_time])
            ->andFilterWhere(['like', 'p_bidcategory', $this->p_bidcategory])
            ->andFilterWhere(['like', 'p_imgpath', $this->p_imgpath]);

        return $dataProvider;
    }
}
