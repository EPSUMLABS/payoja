<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BidPackage;

/**
 * BidPackageSearch represents the model behind the search form of `common\models\BidPackage`.
 */
class BidPackageSearch extends BidPackage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bidpack_id', 'bpk_bids', 'bpk_isdelete'], 'integer'],
            [['bpk_name', 'bpk_price', 'bpk_added_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BidPackage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bidpack_id' => $this->bidpack_id,
            'bpk_bids' => $this->bpk_bids,
            'bpk_added_on' => $this->bpk_added_on,
            'bpk_isdelete' => $this->bpk_isdelete,
        ]);

        $query->andFilterWhere(['like', 'bpk_name', $this->bpk_name])
            ->andFilterWhere(['like', 'bpk_price', $this->bpk_price]);

        return $dataProvider;
    }
}
