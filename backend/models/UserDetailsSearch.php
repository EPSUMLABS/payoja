<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserDetails;

/**
 * UserDetailsSearch represents the model behind the search form of `common\models\UserDetails`.
 */
class UserDetailsSearch extends UserDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'u_accountid', 'u_isconfirmed', 'is_delete'], 'integer'],
            [['u_name', 'u_email', 'u_phoneno', 'u_password', 'uverify_token', 'ufb_token', 'ufb_id', 'u_referalid', 'u_added'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'u_accountid' => $this->u_accountid,
            'u_isconfirmed' => $this->u_isconfirmed,
            'is_delete' => $this->is_delete,
            'uverify_token' => 'fake',
        ]);

        $query->andFilterWhere(['like', 'u_name', $this->u_name])
            ->andFilterWhere(['like', 'u_email', $this->u_email])
            ->andFilterWhere(['like', 'u_phoneno', $this->u_phoneno])
            ->andFilterWhere(['like', 'u_password', $this->u_password])
            ->andFilterWhere(['like', 'uverify_token', $this->uverify_token])
            ->andFilterWhere(['like', 'ufb_token', $this->ufb_token])
            ->andFilterWhere(['like', 'ufb_id', $this->ufb_id])
            ->andFilterWhere(['like', 'u_referalid', $this->u_referalid]);

        return $dataProvider;
    }
}
