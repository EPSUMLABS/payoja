<?php

namespace backend\modules\api\controllers;
use yii\web\Controller;
use Yii;
use common\models\User;
use common\models\UserDetails;
use common\models\Accounts;
use common\models\Referals;
use common\models\BidPackage;
use common\models\Product;
use common\models\BidCategory;
use common\models\BidLogs;
use common\models\UserPayout;
use yii\helpers\Html;
use yii\db\Expression;
use DateTime;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

class AppController extends Controller
{
    public $enableCsrfValidation=false;

    public function send_mail($to,$name,$subject,$body,$altbody){
        $mail = new PHPMailer(true);                             // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'payoja.co@gmail.com';                 // SMTP username
            $mail->Password = 'Poonam@panda1';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to
    
            //Recipients
            $mail->setFrom('payoja.co@gmail.com', 'Payoja');
            $mail->addAddress($to, $name);     // Add a recipient
    
    
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $body;
            $mail->AltBody = $altbody;
    
            $mail->send();
            return true;
        } catch (Exception $e) {
            echo false;
        }
    
    }
    
    public function actionIndex()
    {
         echo "this is test";
        // echo "this is freaking sucking app";
        exit;
        return $this->render('index');
    }

    public function actionAdduser()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
         $UserDetails = new UserDetails();
         $UserDetails->u_name=$attributes['u_name'];
         $UserDetails->u_email=$attributes['u_email'];
         $UserDetails->u_phoneno=$attributes['u_phoneno'];

         $code=substr(md5(mt_rand()),0,15);
         $UserDetails->uverify_token=$code;
         $UserDetails->u_added=date('Y-m-d');
         $password=$attributes['u_password'];
         $UserDetails->u_password=md5($password);
         $chars="01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTYUVWXYZ";
         $referalid=substr(str_shuffle($chars),0,5);
         $UserDetails->u_referalid=$referalid;
         //$UserDetails->attributes= \Yii::$app->request->post();
         if($UserDetails->validate())
         {
            $UserDetails->save();
            $uid=$UserDetails['user_id'];
            Yii::$app->mailer->compose()
            ->setFrom('support@payoja.co')
            ->setTo($UserDetails['u_email'])
            ->setSubject('Account Verification')
            ->setTextBody('Dear User,
             your activation code is "'.$code.'"
             Please click on the below link to verify your account
             http://payoja.co/verify.php?id='.$uid.'&code='.$code.'
             Thanx for using Payoja.')
            ->send();
            // $to=$UserDetails['u_email'];
            // $name=$UserDetails['u_name'];
            // $subject='Account Verification';
            // $from='supprt@payoja.co';
            // $altbody='
            // Dear User,
            //  Your activation code is '.$code.'<br>
            //  Please click on the below link to verify your account<br>
            //  <a href="http://payoja.co/verify.php?id='.$uid.'&code='.$code.'">http://payoja.co/verify.php?id='.$uid.'&code='.$code.'</a><br>
            //  Thanx for using Payoja.
            // ';
            //  $this->send_mail($to,$name,$subject,$altbody,$from);
         
            $Accounts= new Accounts();
            $Accounts->acc_user_id=$UserDetails['user_id'];
           
            $Accounts->acc_freebids=5;
            $Accounts->acc_paidbids=0;
            $Accounts->acc_spins=5;
            $Accounts->acc_credits=0;
            // return array('accounts'=>$Accounts->getErrors());
            if($Accounts->validate())
            {
            $Accounts->save();
            $UserDetails->u_accountid=$Accounts['acc_id'];
            $UserDetails->save();
            return array('status' => 'success','message'=>'Please heck your email for verification and check the spam as well','userdetails'=>$UserDetails,'accounts'=>$Accounts);
         
        }
    }
         else
         {
            return array('status'=>'failed','message'=>'User Email or Phone has already been taken','data'=>$UserDetails->getErrors());
         }
    }
    public function actionUserlogin()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails =  UserDetails::find()->where(['u_email' => $attributes['u_email'],'u_password'=> md5($attributes['u_password']),'is_delete'=>'0','u_isconfirmed'=>'1' ])->one();
       // echo $UserDetails;
        if($UserDetails)
        {
           $referal=Referals::find()->where(['referee_id'=>$UserDetails['user_id']])->one();
           if($referal==null)
           {
            return array('data'=>$UserDetails,'status'=> 'success','message'=>'Login Successful','referal'=>'false');
            // $session = Yii::$app->session;
            // $session->set('userid',$UserDetails['user_id']);
           }
           else{
            return array('data'=>$UserDetails,'status'=> 'success','message'=>'Login Successful','referal'=>'true'); 
           }
        }
        else{
            return array('status'=>'failed','message'=>'Your Account may not have verified yet or please check your email and password');
        }

    }
    
   public function actionChangepassword()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['user_id' => $attributes['user_id'] ])->one();
        $Userpassold=$UserDetails['u_password'];
        //echo $Userpassold.'     ' ;
        $Useroldpass=md5($attributes['u_oldpassword']);
        //echo $Useroldpass;
       $UserDetails->u_password = md5($attributes['u_password']);
        if($Userpassold==$Useroldpass){
            $UserDetails->save();
            return array('status'=> 'success','message'=>'Password Changed');
            }
            else{
                return array('status'=>'failed','message'=>'invalid User Id or Old password');
            }

    }

    public function actionForgotpassword()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['u_email' => $attributes['u_email'] ])->one();
        if($UserDetails){
            $useremail= $UserDetails["u_email"];
            $chars="01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTYUVWXYZ";
            $password=substr(str_shuffle($chars),0,10);
            Yii::$app->mailer->compose()
            ->setFrom('support@payoja.co')
            ->setTo($useremail)
            ->setSubject('Forgot Password')
            ->setTextBody(' Dear User,
            Your password for login. '.$password.'. 
            You can change this after loging into account.
            Thanx for using Payoja.')
            ->send();
            // $to=$useremail;
            // $name=$UserDetails['u_name'];
            // $subject='Forgot Password';
            // $from='payoja.co@gmail.com';
            // $altbody='
            // <h3> Dear User,</h3><br>'.'
            //  Your password for login is '.$password.'<br> 
            // You can change this after loging into account.
            //  Thanx for using Payoja.
            // ';
            //  $this->send_mail($to,$name,$subject,$altbody,$from);
            $UserDetails->u_password=md5($password);
            $UserDetails->save();
            return array('status'=> 'success','message'=>'mail sent');
            }
            else{
                return array('status'=>'failed','message'=>'sorry,please try again.');
            }

    }


    

    public function actionViewuser()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['user_id' => $attributes['user_id'] ])->one();
        if($UserDetails){
            $Accounts = Accounts::find()->where(['acc_user_id'=>$UserDetails["user_id"]])->one();
            $referal=Referals::find()->where(['referee_id'=>$UserDetails['user_id']])->one();
            if($referal==null)
            {
            return array('status'=> 'success','message'=>'Record Found','data'=>$UserDetails,'account'=>$Accounts,'referal'=>'false');
            }
            else{
            return array('status'=> 'success','message'=>'Record Found','data'=>$UserDetails,'account'=>$Accounts,'referal'=>'true');  
            }
        }
            else{
                return array('status'=>'failed','message'=>'invalid user id');
            }

    }

    public function actionEditprofile()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['user_id' => $attributes['user_id'] ])->one();
        if($UserDetails){
            $UserDetails->u_name = $attributes['u_name'];
            $UserDetails->save();
            return array('status'=> 'success','message'=>'Profile Updated','data'=>$UserDetails);
            }
            else{
                return array('status'=>'failed','message'=>'invalid user id');
            }
    }

    public function actionDeleteuser()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
        $attributes= \Yii::$app->request->post();
        $UserDetails = UserDetails::find()->where(['user_id' => $attributes['user_id'] ])->one();
        if($UserDetails)
        {
            $UserDetails->is_delete=1;
            $UserDetails->save();
            $referals=Referals::find()->where(['referrer_id'=>$attributes['user_id']])->all();
            $referallsize=sizeof($referals);
            //echo $referallsize;
            for($i=0;$i<$referallsize;$i++)
            {
                $accuserid=$referals[$i]['referee_id'];
                //echo $accuserid;
                $Accounts=Accounts::find()->where(['acc_user_id'=>$accuserid])->one();
                $oldcredit=$Accounts['acc_credits'];
                if($oldcredit>=500)
                {
                $newcredit=$oldcredit-500;
               // echo $newcredit;
                $Accounts->acc_credits=$newcredit;
                 $Accounts->save();
                }
                else
                {
                    $Accounts->acc_credits=0;
                    $Accounts->save(); 
                }
            }

            $referalsrefree=Referals::find()->where(['referee_id'=>$attributes['user_id']])->one();
            $refereracc=$referalsrefree['referrer_id'];
            $refreeacc=$referalsrefree['referee_id'];
            // echo $refereracc.' '.$refreeacc;
            $RefreeAccounts=Accounts::find()->where(['acc_user_id'=>$refreeacc])->one();
            $refreeoldcredits=$RefreeAccounts['acc_credits'];
             $RefreeAccounts->acc_credits=$refreeoldcredits-500;
             $RefreeAccounts->save();
             $refrerAccounts=Accounts::find()->where(['acc_user_id'=>$refereracc])->one();
             $refreroldcredit=$refrerAccounts['acc_credits'];
             if($refreroldcredit>=500)
             {
             $refrerAccounts->acc_credits=$refreroldcredit-500;
             $refrerAccounts->save();
             }
             else
             {
                $refrerAccounts->acc_credits=0;
                $refrerAccounts->save();

             }
             return array('status'=>'success', 'data'=>'Record Deleted');
        }
        else
        {
            return array('status'=>'failed','data'=>'no records found');
        }

    }
    
    public function actionViewproducts()
    {
      \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;//this will return response as json
       while($rows = (new \yii\db\Query())
      ->select(['p.pid','p.p_name','p.p_price','bc.bidcat_name','p.p_bid_price','p.p_start_time','p.p_imgpath','p.p_addedon'])
      ->from('product p')
      ->join('INNER JOIN', 'bid_category bc', 'p.p_bidcategory = bc.bidcat_id')
      ->all())
      {
      // $arr=array('pid'=>$rows['pid'],'product_name'=>$rows['p_name']);
      return array('prod'=>$rows);
      }
           
    }

    public function actionReferals()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
        $attributes= \Yii::$app->request->post();
        $referalid=$attributes["u_referalid"];
        $referals = UserDetails::find()->where(['u_referalid' => $referalid, 'is_delete'=>'0' ])->one();
        $referrer=$referals["user_id"];
       if($referrer==null)
       {
        return array('status'=>'failed','data'=>'please enter a valid referal id');
       }
       else{
             $referal = new Referals();
            $referal->referrer_id=$referrer;
            $referal->referee_id=$attributes['user_id'];
            $referal->radded_on=date('Y-m-d');
           if($referal->validate()){
                $referal->save();
                $Accounts = Accounts::find()->where(['acc_user_id' => $referrer ])->one();
                $referreracc=$Accounts["acc_credits"];
              
                $val=5;
                $value=$referreracc+$val;
                $Accounts->acc_credits=$value;
               
                $Accounts->save();
               
                $refreeacc=Accounts::find()->where(['acc_user_id' => $attributes['user_id'] ])->one();
                $refreeacc->acc_credits=500;
                
                $refreeacc->save();
                return array('status'=>'success','message'=>'Account Credited');
         }
         else{
            return array('status'=>'failed','message'=>'some problem occured','referal'=>$referal->getErrors() ); 
         }
        }
     }

     public function actionWithdrawl()
     {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
        $attributes= \Yii::$app->request->post();
        $user_id=$attributes["user_id"];
        $Accounts = Accounts::find()->where(['acc_user_id' => $user_id ])->one();
        if($Accounts["acc_credits"]>=50)
        {
            return array('status'=>'success','message'=>'Amount can be withdrawed');
        }
        else{
            return array('status'=>'failed','message'=>'Amount can not be withdrawed'); 
        }
     }

     public function actionViewbidpack()
     {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
        $bidpack = BidPackage::find()->all();
        if($bidpack==null)
        {
            return array('status'=>'failed','message'=>'No records found');   
        }
        else{
            return array('status'=>'success','message'=>'record found','data'=>$bidpack);
        }
         
     }
     public function actionBuybidpack()
     {
         \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
         $attributes= \Yii::$app->request->post();
         $bidpackid=$attributes["bidpack_id"];
         //$bidsbought=$attributes["bidsbought"];
         $bidpack = BidPackage::find()->where(['bidpack_id'=>$bidpackid])->one();  
         $bidsbought=$bidpack['bpk_bids'];
         $bidprice=$bidpack["bpk_price"]; 
        // echo $bidprice;
         $user_id=$attributes["user_id"];
         $useraccc=Accounts::find()->where(['acc_user_id'=>$user_id])->one();
         $useracccred=$useraccc["acc_credits"];
         //echo $useracccred;
        if($useracccred>=$bidprice)
        {
             $oldbids=$useraccc['acc_paidbids'];
             $newbids=$oldbids+$bidsbought;
             $Accounts->acc_paidbids=$newbids;

            $newcredit=$useracccred-$bidprice;
            //echo $newcredit;
            $useraccc->acc_credits=$newcredit;
            $useraccc->save();
            return array('status'=>'success','message'=>'You bought a package using credits','data'=>$useraccc);
        }
        else
        {
            return array('status'=>'failed','message'=>'You dont have enough credits to buy the package');     
        }
     
     
   }

     public function actionCreditbids()
     {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
         $attributes= \Yii::$app->request->post();
         $user_id=$attributes["user_id"];
         $Accounts=Accounts::find()->where(['acc_user_id'=>$user_id])->one();
         date_default_timezone_set("Asia/Calcutta");
         $date=date('Y-m-d H:i:s');
         //echo $date.'   ';
         $lastbidtime=$Accounts["acc_lastfreebid_time"];
         $lastbiddttime=new DateTime($lastbidtime);
         //echo $lastbidtime.'   ';
         $datetime1 = new DateTime('now');
         $interval =  $lastbiddttime->diff($datetime1);
         $timeinterval=$interval->h.':'.$interval->i.':'.$interval->s;
        /// echo $timeinterval;
         $time="01:00:00";
         if(strtotime($timeinterval)>strtotime($time))
         {
            $Accounts->acc_freebids=5;
            $Accounts->acc_lastfreebid_time=$date;
            $Accounts->save();
            return array('status'=>'success','message'=>'account credited','accounts'=>$Accounts,'timeinterval'=>$timeinterval);
         }
         else
         {
            return array('status'=>'failed','message'=>"account is not credited",'accounts'=>$Accounts,'timeinterval'=>$timeinterval);
         }

     }
     public function actionCreditspins()
     {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
         $attributes= \Yii::$app->request->post();
         $user_id=$attributes["user_id"];
         $Accounts=Accounts::find()->where(['acc_user_id'=>$user_id])->one();
         date_default_timezone_set("Asia/Calcutta");
         $lastspintime=$Accounts["acc_lastspintime"];
         $lastspindttm=new DateTime($lastspintime);
         $spintime1=new DateTime('now');
         $interval =  $lastspindttm->diff($spintime1);
         $timeinterval=$interval->h.':'.$interval->i.':'.$interval->s;
         $time="00:15:00";
         if(strtotime($timeinterval)>strtotime($time))
         {
            $Accounts->acc_spins=5;
            $Accounts->acc_lastspintime=date('Y-m-d H:i:s');
            $Accounts->save();
            return array('status'=>'success','message'=>'account credited','accounts'=>$Accounts,'timeinterval'=>$timeinterval);
         }
         else
         {
            return array('status'=>'failed','message'=>"account is not credited",'accounts'=>$Accounts,'timeinterval'=>$timeinterval);
         }

     }

     public function actionSpincredits()
     {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
        $attributes= \Yii::$app->request->post();
        $user_id=$attributes["user_id"];
        $Accounts=Accounts::find()->where(['acc_user_id'=>$user_id])->one();
        date_default_timezone_set("Asia/Calcutta");
        if($Accounts["acc_lastspintime"]=="0000-00-00 00:00:00")
        {
            $Accounts->acc_lastspintime=date('Y-m-d H:i:s');
            $Accounts->save();
        }
        $lastspintime=$Accounts["acc_lastspintime"];
        $lastspindttm=new DateTime($lastspintime);
        $spintime1=new DateTime('now');
        $interval =  $lastspindttm->diff($spintime1);
        $timeinterval=$interval->h.':'.$interval->i.':'.$interval->s;
        $time="00:15:00";
        
        if(strtotime($timeinterval)>=strtotime($time))
        {
           //echo $currenttime;
           return array('status'=>'failed','message'=>'Your spinning time exceeded');
        }
         else
         {
                if($Accounts["acc_spins"]<=0)
            {
                return array('status'=>'failed','message'=>'Please try again after 15 minutes.','accounts'=>$Accounts); 
            }
        else{
            $Accounts->acc_spins=$Accounts["acc_spins"]-1;
            $oldcredit=$Accounts['acc_credits'];
            //echo $oldcredit;
            $acccredits=$attributes["acc_credits"];
            $spincredit=$oldcredit+$acccredits;
            $Accounts->acc_credits=$spincredit;
           
            $Accounts->save();
            return array('status'=>'success','message'=>"Accound credited",'data'=>$Accounts,'timeinterval'=>$timeinterval);
            }
        }
    }

    public function actionDobids()
    {
        \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
        $attributes= \Yii::$app->request->post();
        $userid=$attributes['user_id'];
        $bidvalue=$attributes['bid_value'];
        $product_id=$attributes['product_id'];
         $bidlogs=BidLogs::find()->where(['buser_id'=>$userid,'bid_value'=>$bidvalue,'bid_product_id'=>$product_id])->one();
         $oldbidvalue=$bidlogs['bid_value'];
         echo $oldbidvalue;
        if($oldbidvalue==$bidvalue)
        {
            return array('status'=>'failed','message'=>'You have already used this bidding value for this product');
        }
        else{
        date_default_timezone_set("Asia/Calcutta");
        $bid_logs= new BidLogs();
        $bid_logs->buser_id=$userid;
        $bid_logs->bid_value=$bidvalue;
        $bid_logs->bid_product_id=$product_id;
        $bid_logs->bidding_time=date("Y-m-d H:i:s");

        $fakeuser=UserDetails::find()->where(['uverify_token'=>'fake'])->orderBy(new Expression('rand()'))->one();
        $fakeuid=$fakeuser['user_id'];
          $fake_bid=new BidLogs();
        $fake_bid->buser_id=$fakeuid;
        $fake_bid->bid_value=$bidvalue;
        $fake_bid->bid_product_id=$product_id;
        $fake_bid->bidding_time=date("Y-m-d H:i:s");

      // return array('fakeuser'=>$fake_bid);

        if($bid_logs->validate())
        {
            $Products=Product::find()->where(['pid'=>$product_id])->one();
            $pbidprice=intval($Products['p_bid_price']);
           // echo gettype($pbidprice);
            $accounts=Accounts::find()->where(['acc_user_id'=>$userid])->one();
            $freebids=intval($accounts['acc_freebids']);
            $paidbids=intval($accounts['acc_paidbids']);
            //echo gettype($freebids).' '.gettype($paidbids);
           // $bid_logs->save();
           if($freebids>=$pbidprice)
            {
              $remainingfreebids=$freebids-$pbidprice;
               // echo $remainingfreebids;
                $accounts->acc_freebids=$remainingfreebids;
                $accounts->save();
                $Products->p_lastbid=date("Y-m-d H:i:s");
                $Products->save();
                $bid_logs->save();
                $fake_bid->save();

                return array('status'=>'success','message'=>"Bid value added",'data'=>$bid_logs,'account'=>$accounts);
            }
            elseif($freebids<$pbidprice && $paidbids>=$pbidprice)
            {
                $remainingpaidbids=$paidbids-$pbidprice;
               // echo $remainingpaidbids;
                $accounts->acc_paidbids=$remainingpaidbids;
                $accounts->save();
                $Products->p_lastbid=date("Y-m-d H:i:s");
                $Products->save();
                $bid_logs->save();
                $fake_bid->save();

                return array('status'=>'success','message'=>"Bid value added",'data'=>$bid_logs,'account'=>$accounts);
                
            }
            else{
                return array('status'=>'failed','message'=>"Please Try Again after 1 hour Or You can buy a bid package to use paid bids..");
            }
        }
        else{
            return array('status'=>'failed','message'=>'some error','data'=>$bid_logs->getErrors());
        }

        }
    }


    // public function actionProductview()
    // {
    //     \Yii::$app->response->format= \yii\web\Response::FORMAT_JSON;
    //    $bidcategory=BidCategory::find()->all();
    //     $bidcatsize=sizeof($bidcategory);
    //    // echo $bidcatsize;
    //    $bidcategoryarr=[];
    //    $cat_name=[];
    //    $productsarr=[];
    //     for($i=0; $i<$bidcatsize; $i++)
    //     {
    //          //array_push($bidcategoryarr,array('data'=> $bidcategory[$i]['bidcat_name']));
    //         $products=Product::find()->where(['p_bidcategory'=>$bidcategory[$i]['bidcat_id']])->all();
    //          array_push( $products,array('timeduration'=>$bidcategory[$i]['bid_cat_days'].':'.$bidcategory[$i]['bid_cat_hour'].':'.$bidcategory[$i]['bid_cat_mins'].':'.$bidcategory[$i]['bid_cat_seconds']));
    //          array_push($productsarr,array($bidcategory[$i]['bidcat_name']=>$products));
    //         //  $pro=array($bidcategory[$i]['bidcat_name']=>$productsarr);    
    //     }
    //    return array('products'=>$productsarr);
        
    // }

    public function actionWinner()
    {
        \Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        $attributes= \Yii::$app->request->post();
        $user_id=$attributes['user_id'];
        $products=Product::find()->where(['p_soldto'=>$user_id])->all();
        $productswinner=Product::find()->where(['!=', 'p_soldto', 2])->limit(6)->orderBy(['pid'=>SORT_DESC])->all();
        $productsize=sizeof($productswinner);
        $otherproductwinner=[];
        for($i=0;$i<$productsize;$i++)
        {
        $userdetails=UserDetails::find()->where(['user_id'=>$productswinner[$i]['p_soldto']])->one();
        array_push($otherproductwinner,array('productname'=>$productswinner[$i]['p_name'],'image'=>$productswinner[$i]['p_imgpath'],'winnename'=>$userdetails['u_name']));
        }
       // return array('otherwinner'=>$otherproductwinner);
       $productname=[];
        $count=sizeof($products);
        if($products==NULL)
        {
            return array('status'=>'failed','message'=>'you did not win anything','otherwinner'=>$otherproductwinner);
        }
        else
        {
           for($i=0;$i<$count;$i++)
             {
                $p_starttime=$products[$i]['p_start_time'];
                $bidcategory=BidCategory::find()->where(['bidcat_id'=>$products[$i]['p_bidcategory']])->one();
               //echo $p_starttime;
                $bidcat_days=$bidcategory['bid_cat_days'];
                $bid_cat_hour=$bidcategory['bid_cat_hour'];
                $bid_cat_mins=$bidcategory['bid_cat_mins'];
                $bid_cat_seconds=$bidcategory['bid_cat_seconds'];
               // echo $bidcat_days.'  '.$bid_cat_hour.'  '.$bid_cat_mins.'  '.$bid_cat_seconds;
                $p_endtime = date('Y-m-d H:i:s',strtotime(+$bidcat_days.' day '.+$bid_cat_hour .'hour'.+$bid_cat_mins .' minutes '.+$bid_cat_seconds .'seconds',strtotime($p_starttime)));
                //echo $p_endtime;
                date_default_timezone_set("Asia/Calcutta");
                $current_date=date("Y-m-d H:i:s");
               if( $current_date>$p_endtime)
               {
             array_push($productname,array('products'=>$products[$i]['p_name'],'image'=>$products[$i]['p_imgpath'],'price'=>$products[$i]['p_price']));
             }
            
            }
            if($productname!=NULL)
            {
            return array('status'=>'success','message'=>'you won the bidding','data'=>$productname,'otherwinner'=>$otherproductwinner);
            }
            else
            {
                return array('status'=>'failed','message'=>'you did not win anything','otherwinner'=>$otherproductwinner); 
            } 
        }
    
    
    
    }
    
   public function actionProductview()
    {
        \Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        $con = mysqli_connect("localhost","root","","payoja");
        $bidcatsize=[];
        $bidarray=[];
        $productsarr=[];
        $sqlbid="SELECT * FROM `bid_category`";
        $bidresult=mysqli_query($con,$sqlbid);
        $row_cnt=mysqli_num_rows($bidresult);
      
        for($i=0;$i<$row_cnt;$i++)
       {
         $rowbid=mysqli_fetch_array($bidresult);
          //echo $rowbid['bidcat_name'];
         
            $bidcatid=$rowbid['bidcat_id']; 
            $bid_name=$rowbid['bidcat_name'];
         $sqlprod="SELECT `pid`,`p_name`,`p_price`,`p_bid_price`,`u_name`,`p_start_time`,`bidcat_name`,`p_imgpath`,`p_addedon`,`bid_cat_days`,`bid_cat_hour`,`bid_cat_mins`,`bid_cat_seconds` FROM `product` INNER JOIN bid_category ON product.`p_bidcategory`=bid_category.bidcat_id INNER JOIN user_details ON product.p_soldto=user_details.user_id where p_bidcategory=$bidcatid";
         $result=mysqli_query($con,$sqlprod);
         $productsarr=[];
           while($row=mysqli_fetch_array($result))
           {
            date_default_timezone_set("Asia/Calcutta");
            $current_date=date("Y-m-d H:i:s");
            
             $p_starttime= $row['p_start_time'];
             $pid=$row['pid'];
             $bid_days= $row['bid_cat_days'];
             $bid_hour= $row['bid_cat_hour'];
             $bid_mins= $row['bid_cat_mins'];
             $bid_second= $row['bid_cat_seconds'];
             $p_endtime = date('Y-m-d H:i:s',strtotime(+$bid_days.' day '.+$bid_hour .'hour'.+$bid_mins .' minutes '.+$bid_second .'seconds',strtotime($p_starttime)));
            
             
            
             $differenceInSeconds=strtotime($p_endtime)-strtotime($current_date);
             if($current_date>=$p_starttime)
             {
             array_push($productsarr,array('pid'=>$pid,
             'p_name'=>$row['p_name'],'p_price'=>$row['p_price'],'p_bid_price'=>$row['p_bid_price'],'p_soldto'=>$row['u_name'],
             'p_start_time'=>$row['p_start_time'],'bidcat_name'=>$row['bidcat_name'],'p_imgpath'=>$row['p_imgpath'],'timeduration'=>$differenceInSeconds));
             }
             
           }
           array_push($bidcatsize,array($bid_name=>$productsarr));
          
      } 
     
    return array('status'=>'success','message'=>'record found','Products'=>$bidcatsize);
    }
    
    
    
    public function actionPayout()
    {
        \Yii::$app->response->format=\yii\web\Response::FORMAT_JSON; 
        $attributes= \Yii::$app->request->post();
        $Payout = new UserPayout();
        date_default_timezone_set("Asia/Calcutta");
        $Payout->user_id=$attributes['user_id'];
        $Payout->paytm_phoneno=$attributes['paytm_phoneno'];
        $Payout->user_name=$attributes['user_name'];
      //  echo  $Payout['user_name'];
        $Payout->user_bankacc_no=$attributes['user_bankacc_no'];
        $Payout->user_bankifsc_code=$attributes['user_bankifsc_code'];
        $Payout->amount=$attributes['amount']/100;
       // return array('payout'=>$Payout);

        $chars="01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTYUVWXYZ";
        $transaction_id='txn'.substr(str_shuffle($chars),0,5);
        $Payout->transaction_id=$transaction_id;

        $accounts=Accounts::find()->where(['acc_user_id'=>$attributes['user_id']])->one();  
        $acccredits=$accounts['acc_credits'];
       // echo $acccredits.'<br>';
        $newacccredits=$acccredits-$attributes['amount'];
       // echo $newacccredits;
       $accounts->acc_credits=$newacccredits;
       $accounts->save();
        $Payout->status='pending';
        $Payout->date_of_request=date('Y-m-d H:i:s');
        if($Payout->validate())
        {
            $Payout->save();
            return array('status'=>'success','message'=>'Payout Request Given','data'=>$Payout,'account'=>$accounts);
        }
        else
        {
            return array('status'=>'failed','message'=>'Payout Request Failed','data'=>$Payout->getErrors());
        }


    }
    
      public function actionBuybidpackage()
    {
        \Yii::$app->response->format=\yii\web\Response::FORMAT_JSON; 
         $attributes= \Yii::$app->request->post();
         $user_id=$attributes["user_id"];
         $bidpackid=$attributes["bidpack_id"];
         $bidpack = BidPackage::find()->where(['bidpack_id'=>$bidpackid])->one();  
         $bidsbought=$bidpack['bpk_bids'];
         $Accounts = Accounts::find()->where(['acc_user_id'=>$user_id])->one();
         $oldbids=$Accounts['acc_paidbids'];
         //echo $oldbids;
         $newbids=$oldbids+$bidsbought;
         //echo $newbids;
         $Accounts->acc_paidbids=$newbids;
         if($Accounts->save())
         {
            return array('status'=>'success','message'=>'Account Bids Updated','data'=>$Accounts);  
         }
         else
         {
            return array('status'=>'failed','message'=>'Account updation failed','data'=>$Accounts->getErrors());
         }


    }

    public function actionAddproducts()
    {
        \Yii::$app->response->format=\yii\web\Response::FORMAT_JSON; 
         $attributes= \Yii::$app->request->post();
         $Product = new Product();
         $Product->p_name=$attributes['p_name'];
         $Product->p_price=$attributes['p_price'];
         $Product->p_bid_price=$attributes['p_bid_price'];
         $Product->p_start_time=$attributes['p_start_time'];
         $Product->p_bidcategory=$attributes['p_bidcategory'];
         $Product->p_imgpath=$attributes['p_imgpath'];
         $Product->p_soldto=$attributes['p_soldto'];
         $Product->p_addedon=$attributes['p_addedon'];
         $Product->p_isdelete='0';
         if($Product->validate())
         {
            $Product->save();
            return array('status'=>'success','message'=>'Products Added','data'=>$Product);  
         }
         else
         {
            return array('status'=>'failed','message'=>'Product Addition Failed','data'=>$Product->getErrors());   
         }

    }
   
    public function actionAdminresetpass()
    {
        \Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        $attributes=\Yii::$app->request->post();
        $user_email=$attributes['u_email'];
        $userpassword=$attributes['u_password'];
        $userdetails=UserDetails::find()->where(['u_email'=>$user_email])->one();
        if($userdetails==null)
        {
            return array('status'=>'failed','message'=>'Please enter the correct email address');
        }
        else{
        $userdetails->u_password=md5($userpassword);
        if($userdetails->validate())
        {
            $userdetails->save();
            return array('status'=>'success','message'=>'New Password For the user Saved');
        }
        else
        {
            return array('status'=>'failed','message'=>'Some Error Occured','data'=>$userdetails->getErrors());
        }
    }
    }
     
}

