<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>-->
    <?= Html::csrfMetaTags() ?>
    <title>Payoja</title>
    <?php $this->head() ?>
    <script>
    function addnewproduct()
    {
        var p_name=$('input[name=p_name]').val();
        var p_price=$('input[name=p_price]').val();
        var p_bid_price=$('input[name=p_bid_price]').val();
        var p_start_time=$('input[name=p_start_time]').val();
        var p_bidcategory=$('input[name=p_bidcategory]').val();
        var p_imgpath=$('input[name=p_imgpath]').val();
        var p_soldto=$('input[name=p_soldto]').val();
        var p_addedon=$('input[name=p_addedon]').val();
        var formdata={
            'p_name':p_name,
            'p_price':p_price,
            'p_bid_price':p_bid_price,
            'p_start_time':p_start_time,
            'p_bidcategory':p_bidcategory,
            'p_imgpath':p_imgpath,
            'p_soldto':p_soldto,
            'p_addedon':p_addedon,
        }
        console.log(formdata);
        $.ajax({
            url: 'http://payoja.co/backend/web/index.php/api/app/addproducts',
            type: 'POST',
            
            data:formdata,
			dataType: 'json'
		})
        .done(function(data){
		console.log(data)
        window.location.href = 'http://localhost/payoja/backend/web/index.php/product/index';
        })
        // bookfunction(available);
      
        }

        function resetpassword()
        {
         var user_email=$('#uemail').val();
        // var user_email=document.getElementById("uemail");
        // console.log(user_email);
         var user_password=$('#upassword').val();
         var formdata={
             'u_email':user_email,
             'u_password':user_password,
         }
         console.log(formdata);    
         $.ajax({
            url: 'http://localhost/payoja/backend/web/index.php/api/app/adminresetpass',
            type: 'POST',
            
            data:formdata,
			dataType: 'json'
		})
        .done(function(data){
		//console.log(data)
        if(data.status=='success')
        {
            window.alert(data.message);
            location.reload();
        }
        else
        {
            window.alert(data.message); 
        }
        })

        }
       
    
    </script>
   
</head>
<body style="overflow-x:hidden;" class="" data-gr-c-s-loaded="true">
<?php $this->beginBody() ?>
<nav class="navbar collapse navbar-collapse" style="background-color:#6739b6">
        <div class="container-fluid">
        <div class="navbar-header">
                <?= Html::a('PAYOJA', ['/site/index'],['class'=>'navbar-brand','style' => 'font-weight:bold;color:#fff']) ?></span>
            </div>
            <div class="nav navbar-nav navbar-right">
            <?= Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'LOGOUT (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout','style'=>'font-weight:bold;color:#fff;padding-top:24px']
            )
             . Html::endForm()?>
            </div>
            </nav>
 <section>
        <!-- Left Sidebar -->
        <aside  class="sidebar" style="overflow-y:scroll;display: inline-block;">
            <!-- User Info -->
            <div class="user-info" style="height:250px">
                <div class="image">
                <img src= '../../images/logo.png' style="height:200px;width:250px"/>
                </div>
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu" style="position: sticky;">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                   
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">home</i>
                    <?= Html::a('Home', ['../../web'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                        
                    </li>
                    <li>
                       
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">person_add</i>
                            <?= Html::a('Create User', ['/user-details/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                    </li>

                     <li>
                       
                       <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">person</i>
                               <?= Html::a('User Details', ['/real-user-details/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       </li>

                    <li>
                    <li>
                       
                       <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">lock</i>
                               <?= Html::a('Reset Userpassword', ['/user-details/resetpass'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       </li>

                    <li>
                       
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">bookmark</i>
                            <?= Html::a('Products', ['/product/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                    </li>
                    <li>
                        
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">list</i>
                    <?= Html::a('Bid Category', ['/bid-category/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       
                    </li>
                    <li>
                        
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">library_add</i>
                    <?= Html::a('Bid Packages', ['/bid-package/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       
                    </li>

                     <li>
                        
                        <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">payment</i>
                        <?= Html::a('Pay Requests', ['/user-payout/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                           
                        </li>
                        <li>
                        
                        <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">star</i>
                        <?= Html::a('Winners', ['/site/winner'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                           
                        </li>
                   
                   
                   
                </ul>
            </div>
        </aside>
<section class="content">
<div class="container-fluid">

   

    <div>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
</section>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
