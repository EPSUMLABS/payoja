<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\DataAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

DataAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?= Html::csrfMetaTags() ?>
    <title>Payoja</title>
    <?php $this->head() ?>
   
</head>
<body style="overflow-x:hidden;">
<?php $this->beginBody() ?>
<nav class="navbar collapse navbar-collapse" style="background-color:#6739b6">
        <div class="container-fluid">
        <div class="navbar-header">
                <?= Html::a('PAYOJA', ['/site/index'],['class'=>'navbar-brand','style' => 'font-weight:bold;color:#fff']) ?></span>
            </div>
            <div class="nav navbar-nav navbar-right">
            <?= Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'LOGOUT (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout','style'=>'font-weight:bold;color:#fff;padding-top:24px']
            )
             . Html::endForm()?>
            </div>
            </nav>
 <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="height:250px">
                <div class="image">
                <img src= '../../images/logo.png' style="height:200px;width:250px"/>
                </div>
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                   
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">home</i>
                    <?= Html::a('Home', ['../../web'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                        
                    </li>
                    <li>
                       
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">person_add</i>
                            <?= Html::a('Create User', ['/user-details/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                    </li>

                     <li>
                       
                       <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">person</i>
                               <?= Html::a('User Details', ['/real-user-details/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       </li>

                    <li>
                    <li>
                       
                       <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">lock</i>
                               <?= Html::a('Reset Userpassword', ['/user-details/resetpass'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       </li>

                    <li>
                       
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">bookmark</i>
                            <?= Html::a('Products', ['/product/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                    </li>
                    <li>
                        
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">list</i>
                    <?= Html::a('Bid Category', ['/bid-category/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       
                    </li>
                    <li>
                        
                    <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">library_add</i>
                    <?= Html::a('Bid Packages', ['/bid-package/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                       
                    </li>

                     <li>
                        
                        <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">payment</i>
                        <?= Html::a('Pay Requests', ['/user-payout/index'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                           
                        </li>
                        <li>
                        
                        <span style="display:flex;padding-top:10px"><i class="material-icons" style="margin-left:15px">star</i>
                        <?= Html::a('Winners', ['/site/winner'],['style' => 'font-weight:bold;color:#333333']) ?></span>
                           
                        </li>
                   
                   
                   
                </ul>
            </div>
        </aside>
<section class="content">
<div class="container-fluid">

   

    <div>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
</section>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
