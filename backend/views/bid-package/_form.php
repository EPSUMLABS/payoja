<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BidPackage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bid-package-form">
<div class="row">

<div class="col-lg-1"></div>
<div class="col-lg-10">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
<div class="col-lg-6">
    <?= $form->field($model, 'bpk_name')->textInput(['maxlength' => true,'style'=>'height:35px;font-size:15px;']) ?>
</div>
<div class="col-lg-6">
    <?= $form->field($model, 'bpk_bids')->textInput(['style'=>'height:35px;font-size:15px;']) ?>
</div>
</div>

<div class="row">
<div class="col-lg-6">
    <?= $form->field($model, 'bpk_price')->textInput(['maxlength' => true,'style'=>'height:35px;font-size:15px;']) ?>
</div>
</div>
    <?php //$form->field($model, 'bpk_added_on')->textInput() ?>

    <?php //$form->field($model, 'bpk_isdelete')->textInput() ?>

    <div class="form-group">
       <center> <?= Html::submitButton('Save', ['class' => 'btn btn-success','style'=>'width:40%']) ?></center>
        </div>
    </div>
</div>
</div>
    <?php ActiveForm::end(); ?>

</div>
