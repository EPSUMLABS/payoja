<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BidPackageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bid-package-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'bidpack_id') ?>

    <?= $form->field($model, 'bpk_name') ?>

    <?= $form->field($model, 'bpk_bids') ?>

    <?= $form->field($model, 'bpk_price') ?>

    <?= $form->field($model, 'bpk_added_on') ?>

    <?php // echo $form->field($model, 'bpk_isdelete') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
