<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BidPackage */

$this->title = 'Create Bid Package';
$this->params['breadcrumbs'][] = ['label' => 'Bid Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<div class="bid-package-create">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
</div>
