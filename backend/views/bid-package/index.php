<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BidPackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bid Packages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<div class="bid-package-index">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Bid Package', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
<div class="body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'bidpack_id',
            'bpk_name',
            'bpk_bids',
            'bpk_price',
            'bpk_added_on',
            //'bpk_isdelete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>