<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BidPackage */

$this->title = "View Bid Category";
$this->params['breadcrumbs'][] = ['label' => 'Bid Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<div class="bid-package-view">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="body">
   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bidpack_id',
            'bpk_name',
            'bpk_bids',
            'bpk_price',
            'bpk_added_on',
            //'bpk_isdelete',
        ],
    ]) ?>

</div>
</div>
</div>