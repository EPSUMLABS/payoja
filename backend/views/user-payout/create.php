<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserPayout */

$this->title = 'Create User Payout';
$this->params['breadcrumbs'][] = ['label' => 'User Payouts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-payout-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
