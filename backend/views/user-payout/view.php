<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserPayout */

$this->title = $model->payout_id;
$this->params['breadcrumbs'][] = ['label' => 'User Payouts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-payout-view">

    <h1><?= Html::encode($this->title) ?></h1>

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'payout_id',
            'user_id',
            'user_bankacc_no',
            'user_bankifsc_code',
            'amount',
            'transaction_id',
            'status',
            'date_of_request',
        ],
    ]) ?>

</div>
