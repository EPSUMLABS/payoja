<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserPayoutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Payouts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<div class="user-payout-index">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <div class="body">
   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'payout_id',
            //'user_id',
            'user_name',
            'paytm_phoneno',
            'user_bankacc_no',
            'user_bankifsc_code',
            'amount',
            'transaction_id',
            'status',
            'date_of_request',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{link}',
                'buttons' => [
                    'link' => function ($url,$model,$key) {
                        return Html::a('Approve','http://localhost/payoja/approve.php?id='.$model->payout_id.'',
                        ['class' => 'btn btn-primary']);
                    }
                ],
            ],
        ],
    ]); 
    ?>
    </div>
    </div>
    </div>
    