<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserPayout */

$this->title = 'Update User Payout: ' . $model->payout_id;
$this->params['breadcrumbs'][] = ['label' => 'User Payouts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->payout_id, 'url' => ['view', 'id' => $model->payout_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-payout-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
