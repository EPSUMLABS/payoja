<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserPayoutSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-payout-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'payout_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'user_bankacc_no') ?>

    <?= $form->field($model, 'user_bankifsc_code') ?>

    <?= $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'transaction_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'date_of_request') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
