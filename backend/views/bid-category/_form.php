<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BidCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bid-category-form">
<div class="row">

<div class="col-lg-1"></div>
<div class="col-lg-10">
    <?php $form = ActiveForm::begin(); ?>
<div class="row">
<div class="col-lg-2"></div>
<div class="col-lg-8">
   <?= $form->field($model, 'bidcat_name')->textInput(['maxlength' => true,'style'=>'height:35px;font-size:15px;']) ?>
    </div>  
    
</div>

<div class="row">
<center><label>Time Duration</label></center>
<div class="col-lg-2"></div>
<center>

<div class="col-lg-2">
    <?= $form->field($model, 'bid_cat_days')->textInput(['style'=>'height:35px;font-size:15px;','type'=>'number'])->label('') ?>
<label>Days</label>
</div>
<div class="col-lg-2">
    <?= $form->field($model, 'bid_cat_hour')->textInput(['style'=>'height:35px;font-size:15px;','type'=>'number'])->label(' ') ?>
    <label>Hours</label>
    </div>  
    
    <div class="col-lg-2">
    <?= $form->field($model, 'bid_cat_mins')->textInput(['style'=>'height:35px;font-size:15px;','type'=>'number'])->label(' ') ?>
    <label>Minute</label>
    </div>
   
    <div class="col-lg-2">
    <?= $form->field($model, 'bid_cat_seconds')->textInput(['style'=>'height:35px;font-size:15px;','type'=>'number'])->label(' ') ?>
    <label>Seconds</label> 
    </div>
   
    </center>

</div>

    <?php //$form->field($model, 'bidcat_addedon')->textInput(['style'=>'height:35px;font-size:15px;']) ?>
    </div>
    <div class="form-group">
       <center> <?= Html::submitButton('Save', ['class' => 'btn btn-success','style'=>'width:40%']) ?></center>
    </div>
    </div>
</div>
</div>

    <?php ActiveForm::end(); ?>

</div>
