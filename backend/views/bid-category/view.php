<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BidCategory */

$this->title = "View Bid Category";
$this->params['breadcrumbs'][] = ['label' => 'Bid Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" >
<div class="bid-category-view">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="body">
   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bidcat_id',
            'bidcat_name',
            'bidcat_addedon',
            'bid_cat_days',
            'bid_cat_hour',
            'bid_cat_mins',
            'bid_cat_seconds',
        ],
    ]) ?>
</div>
</div>
</div>
