<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BidCategory */

$this->title = 'Update Bid Category: ' . $model->bidcat_id;
$this->params['breadcrumbs'][] = ['label' => 'Bid Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bidcat_id, 'url' => ['view', 'id' => $model->bidcat_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bid-category-update">
<div class="card">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
</div>
