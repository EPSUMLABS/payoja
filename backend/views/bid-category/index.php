<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BidCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bid Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<div class="bid-category-index">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
<div class="body">
    <p>
        <?= Html::a('Create Bid Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'bidcat_id',
            'bidcat_name',
            'bidcat_addedon',

            ['class' => 'yii\grid\ActionColumn'],
           
        ],
    ]); ?>
</div>
</div>
</div>
