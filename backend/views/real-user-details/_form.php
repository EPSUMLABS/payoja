<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'u_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_phoneno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_accountid')->textInput() ?>

    <?= $form->field($model, 'uverify_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ufb_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ufb_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_referalid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_isconfirmed')->textInput() ?>

    <?= $form->field($model, 'is_delete')->textInput() ?>

    <?= $form->field($model, 'u_added')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
