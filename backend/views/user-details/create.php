<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserDetails */

$this->title = 'Create User Details';
$this->params['breadcrumbs'][] = ['label' => 'User Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-details-create">
<div class="card">
<div class="header">

    <h1><?= Html::encode($this->title) ?></h1>
    </div>
<div class="body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


  </div>
</div>
</div>
