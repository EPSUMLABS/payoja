<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserDetails */

$this->title = $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'User Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" >
<div class="user-details-view">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
<div class="body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'u_name',
            'u_email:email',
            'u_phoneno',
           // 'u_password',
           // 'u_accountid',
            //'uverify_token',
            //'ufb_token',
           // 'ufb_id',
            //'u_referalid',
           // 'u_isconfirmed',
           // 'is_delete',
            'u_added',
        ],
    ]) ?>

</div>
</div>
</div>
