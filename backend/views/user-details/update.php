<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserDetails */

$this->title = 'Update User Details: ' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'User Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-details-update">
<div class="card">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
<div class="body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


  </div>
</div>
</div>
