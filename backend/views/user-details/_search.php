<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'u_name') ?>

    <?= $form->field($model, 'u_email') ?>

    <?= $form->field($model, 'u_phoneno') ?>

    <?= $form->field($model, 'u_password') ?>

    <?php // echo $form->field($model, 'u_accountid') ?>

    <?php // echo $form->field($model, 'uverify_token') ?>

    <?php // echo $form->field($model, 'ufb_token') ?>

    <?php // echo $form->field($model, 'ufb_id') ?>

    <?php // echo $form->field($model, 'u_referalid') ?>

    <?php // echo $form->field($model, 'u_isconfirmed') ?>

    <?php // echo $form->field($model, 'is_delete') ?>

    <?php // echo $form->field($model, 'u_added') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
