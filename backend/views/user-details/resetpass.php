<?php
use yii\helpers\Html;

$this->title = 'Reset Password';
?>
<div class="user-details-resetpass">
<div class="card">
<div class="header">
<h2>Reset Password</h2>
</div>
<div class="body">
<form name="resetpass" enctype="multipart/form-data" method="post">
<div class="row">
<div class="col-lg-1"></div>
<div class="col-lg-10">
<div class="row">
<div class="col-lg-6">
<label>User Email</label>
<input type="text" class="form-control" name="uemail" id="uemail" >
</div>
<div class="col-lg-6">
<label>User Password</label>
<input type="password" class="form-control" name="upassword" id='upassword' >
</div>
</div>
<center><button type="button" onclick="resetpassword();" name="resetpassbtn"  class="btn btn-success" >Submit</button></center>
</form>
</div>
</div>