<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-details-form">
<div class="row">

<div class="col-lg-1"></div>
<div class="col-lg-10">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
<div class="col-lg-6">
    <?= $form->field($model, 'u_name')->textInput(['maxlength' => true,'placeholder' => 'Enter a User Name']) ?>
    </div>
<div class="col-lg-6">
    <?= $form->field($model, 'u_email')->textInput(['maxlength' => true, 'placeholder'=>'Enter a Uniue Email']) ?>
    </div>
    
</div>
    <div class="row">
    <div class="col-lg-6">
    <?= $form->field($model, 'u_phoneno')->textInput(['maxlength' => true,'placeholder'=>'Enter a uniue phone no']) ?>
    </div>
    </div>

    <?= $form->field($model, 'u_password',['inputOptions' => ['value' => 'NA']])->hiddenInput()->label(false)?>

    <?= $form->field($model, 'u_accountid',['inputOptions' => ['value' => 0]])->hiddenInput()->label(false)?>

    <?= $form->field($model, 'uverify_token',['inputOptions' => ['value' => 'fake']])->hiddenInput()->label(false)?>

     <?= $form->field($model, 'ufb_token',['inputOptions' => ['value' => 'NA']])->hiddenInput()->label(false)?>

    <?= $form->field($model, 'ufb_id',['inputOptions' => ['value' => 'NA']])->hiddenInput()->label(false)?>

    <?php
   
    //echo $referalid;
    //$form->field($model, 'u_referalid',['inputOptions' => ['value' => $referalid]])->hiddenInput()->label(false)
    ?>

    <?= $form->field($model, 'u_isconfirmed',['inputOptions' => ['value' => 1]])->hiddenInput()->label(false)?>

    <?= $form->field($model, 'is_delete',['inputOptions' => ['value' => 1]])->hiddenInput()->label(false)?>

    <?= $form->field($model, 'u_added',['inputOptions' => ['value' => date('Y-m-d')]])->hiddenInput()->label(false)?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','style'=>'width:60%']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
