<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<div class="user-details-index">
<div class="header">


    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="body">
    <p>
        <?= Html::a('Create User ', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'u_name',
            'u_email:email',
            'u_phoneno',
            //'u_password',
            //'u_accountid',
            //'uverify_token',
            //'ufb_token',
            //'ufb_id',
            //'u_referalid',
            //'u_isconfirmed',
            //'is_delete',
            //'u_added',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>
