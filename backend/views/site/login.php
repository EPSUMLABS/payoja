<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
<div class="row">
   
     <div class="col-lg-4">
     <!--<img src='../../images/boat_logo1.png'>-->
        <div class='card'>
        <div class='header'>
        <center><h1><?= Html::encode($this->title) ?></h1>
        <p>Please fill out the following fields to login:</p> </center>

        </div>
        <div class='body'>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'style'=>'height:35px;font-size:15px;']) ?>

                <?= $form->field($model, 'password')->passwordInput(['style'=>'height:35px;font-size:15px;']) ?>

                <?php //$form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                  <center><?= Html::submitButton('LOGIN', ['class' => 'btn btn-primary', 'name' => 'login-button','style'=>'width:85%;height:35px']) ?>
                </center>
                </div>
                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['../../../frontend/web/index.php/site/request-password-reset']) ?>.
                </div>

            <?php ActiveForm::end(); ?>
            </div>
            </div>
        </div>
    </div>
</div>
