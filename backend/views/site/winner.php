<?php
use yii\helpers\Html;

$this->title = 'Winner';
?>
<div class="site-winner">


<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Winners
                            </h2>
                          
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>User Name</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Product Name</th>
                                            <th>Product Started</th>
                                            <th>Product Ended</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   
                                 <tbody>
                                 <?php
                                    $con = mysqli_connect("localhost","root","","payoja");
                                    $productsql="SELECT * FROM `user_details` INNER JOIN product ON product.p_soldto=user_details.user_id where uverify_token!='fake'";
                                    $resultuser=mysqli_query($con,$productsql);
                                    while($row=mysqli_fetch_array($resultuser))
                                    {
                                        $pid=$row['pid'];
                                        $p_starttime=$row['p_start_time'];
                                        //echo $p_starttime.'<br>';
                                        $pbidcategory=$row['p_bidcategory'];
                                        $sqlbid="SELECT DISTINCT bid_cat_days,bid_cat_hour,bid_cat_mins,bid_cat_seconds FROM product INNER JOIN bid_category on product.p_bidcategory=bid_category.bidcat_id where bidcat_id='$pbidcategory'   ";
                                        //echo $sqlbid.'<br>';
                                        $resultbid=mysqli_query($con,$sqlbid);
                                        // $bidrows=mysqli_num_rows($resultbid);
                                       while($rowbid=mysqli_fetch_array($resultbid))
                                        {
                                            $_bidcat_days=$rowbid['bid_cat_days'];
                                            $_bidcat_hour=$rowbid['bid_cat_hour'];
                                            $_bidcat_mins=$rowbid['bid_cat_mins'];
                                            $_bidcat_seconds=$rowbid['bid_cat_seconds'];
                                           // echo  $_bidcat_days.'<br>';
                                            $p_endtime = date('Y-m-d H:i:s',strtotime(+$_bidcat_days.' day '.+$_bidcat_hour .'hour'.+$_bidcat_mins .' minutes '.+$_bidcat_seconds .'seconds',strtotime($p_starttime)));
                                           // echo $p_endtime.'<br>';
                                            date_default_timezone_set("Asia/Calcutta");
                                            $current_date=date("Y-m-d H:i:s");
                                            if($current_date>$p_endtime)
                                            {
                                        ?>
                                        <tr>
                                            <td><?php echo $row['u_name']?></td>
                                            <td><?php echo $row['u_email']?></td>
                                            <td><?php echo $row['u_phoneno']?></td>
                                            <td><?php echo $row['p_name']?></td>
                                            <td><?php echo $p_starttime?></td>
                                            <td><?php echo $p_endtime?></td>
                                            <td><?php echo $row['is_given']?></td>
                                            <td><a class="btn" href="http://localhost/payoja/given.php?id=<?php echo $pid?>">Given</a></td>
                                            
                                        </tr>
                                    <?php 
                                            }
                                            }
                                             }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</div>
