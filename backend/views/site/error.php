<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
<center>
<div class="card" style="width:500px;margin-top:150px">
  <div class="body">
   <h1><?php //Html::encode($this->title) ?></h1>

  
  <p style="font-size:25px"><?= nl2br(Html::encode($message)) ?></p>
  
    <!-- <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p></center> -->
</div>
</div>
</div>
