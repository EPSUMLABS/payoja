<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = 'Create Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">
<div class="card">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
<div class="body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

  </div>
</div>
</div>
