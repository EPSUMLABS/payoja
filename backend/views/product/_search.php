<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pid') ?>

    <?= $form->field($model, 'p_name') ?>

    <?= $form->field($model, 'p_price') ?>

    <?= $form->field($model, 'p_bid_price') ?>

    <?= $form->field($model, 'p_soldto') ?>

    <?php // echo $form->field($model, 'p_start_time') ?>

    <?php // echo $form->field($model, 'p_bidcategory') ?>

    <?php // echo $form->field($model, 'p_lastbid') ?>

    <?php // echo $form->field($model, 'p_imgpath') ?>

    <?php // echo $form->field($model, 'p_addedon') ?>

    <?php // echo $form->field($model, 'p_isdelete') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
