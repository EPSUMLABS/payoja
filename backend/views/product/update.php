<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = 'Update Product: ' . $model->pid;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pid, 'url' => ['view', 'id' => $model->pid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">
<div class="card">
<div class="header">

    <h1><?= Html::encode($this->title) ?></h1>
    </div>
<div class="body">
    <?= $this->render('_form1', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
