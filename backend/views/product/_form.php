<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\BidCategory;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">
<div class="row">

<div class="col-lg-1"></div>
<div class="col-lg-10">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
<div class="row">
<div class="col-lg-6">
    <?= $form->field($model, 'p_name')->textInput(['maxlength' => true ,'style'=>'height:35px;font-size:15px;']) ?>
</div>
<div class="col-lg-6">
    <?= $form->field($model, 'p_price')->textInput(['maxlength' => true ,'style'=>'height:35px;font-size:15px;']) ?>
</div>
</div>

<div class="row">
<div class="col-lg-6">
    <?= $form->field($model, 'p_bid_price')->textInput(['maxlength' => true ,'style'=>'height:35px;font-size:15px;']) ?>
    </div>
    
<div class="col-lg-6">


<?= $form->field($model, 'p_start_time')->textInput(['maxlength' => true ,'style'=>'height:35px;font-size:15px;','type'=>'datetime-local','name'=>'pstarttime']) ?>
    </div>
</div>

<div class="row">
<div class="col-lg-6">
<?php //$form->field($model, 'p_bidcategory')->textInput(['style'=>'height:35px;font-size:15px;']) ?>
<?= $form->field($model, 'p_bidcategory')->dropDownList(
        ArrayHelper::map(BidCategory::find()->all(),'bidcat_id','bidcat_name'),
        ['prompt'=>'Select Bidding Category','style'=>'height:35px;font-size:15px;']
    ) ?>
    </div>
<div class="col-lg-6">
<?php //$form->field($model, 'p_lastbid')->textInput(['style'=>'height:35px;font-size:15px;']) ?>
<?= $form->field($model, 'file')->fileInput()?>
    </div>
</div>
<div class="row">
<div class="col-lg-6">

    </div>
    <?= $form->field($model, 'p_soldto',['inputOptions' => ['value' => 1]])->hiddenInput()->label(false)?>
</div>
    <?php //$form->field($model, 'p_addedon')->textInput(['style'=>'height:35px;font-size:15px;border:0.2px solid #bbbbbb']) ?>

    <?php //$form->field($model, 'p_isdelete')->textInput(['style'=>'height:35px;font-size:15px;border:0.2px solid #bbbbbb']) ?>

    <div class="form-group">
       <center> <?= Html::submitButton('Save', ['class' => 'btn btn-success','style'=>'width:60%']) ?></center>
        </div>
    </div>
</div>
</div>

    <?php ActiveForm::end(); ?>

</div>
