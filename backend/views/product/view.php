<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = "View Product";
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card" >
<div class="product-view">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
<div class="body">
   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'pid',
            'p_name',
            'p_price',
            'p_bid_price',
            //'p_soldto',
            'p_start_time',
            'pBidcategory.bidcat_name',
            'p_lastbid',
            ['label' => 'Product Image',

            'format' => ['image',['width'=>'80','height'=>'80']], 

            'value'=>function($model){

            return('@web/'.$model->p_imgpath);

                },

            ],
            //'p_imgpath',    
            'p_addedon',
            //'p_isdelete',
        ],
    ]) ?>

</div>
</div>
</div>
