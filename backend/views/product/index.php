<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<div class="product-index">
<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
<div class="body">
    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pid',
            'p_name',
            'p_price',
            'p_bid_price',
            //'p_soldto',
            'p_start_time',
           // 'p_bidcategory',
            //'p_lastbid',
            //'p_imgpath',
            //'p_addedon',
            //'p_isdelete',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}{link}',
                'buttons' => [
                    'link' => function ($url,$model,$key) {
                        return Html::a( '<span class="glyphicon glyphicon-plus"></span>','http://localhost/payoja/backend/web/index.php/product/renew?id='.$model->pid.''
                        );
                    }
                ],
            ],
        ],
    ]); ?>
</div>
</div>
</div>
