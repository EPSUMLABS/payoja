<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','winner'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
      //  $this->layout ='errorLayout';
        return [
            'error' => [
               // $this->layout ='errorLayout',
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout ='allpageLayout';
        return $this->render('index');
    }

    public function actionWinner()
    {
        $this->layout='dataLayout';
        return $this->render('winner');
    }
    

    /**
     * Login action.
     *
     * @return string
     */

    public function actionLogin()
    {   $action=$_GET["action"];
        if($action=='D56B699830E77BA53855679CB1D252DA')
        {
        $this->layout ='loginLayout';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
       
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    else
    {
        return array(
          
            'components'=>array(
                'errorHandler'=>array(
                    'errorAction'=>'site/error',
                ),
            ),
        );
        // echo "<center style='padding-top:100px;font-size:40px;color:red'><B>404 NOT FOUND</B></center>";   
    }
}
   

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
