<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/bootstrap.css',
        'css/waves.css',
        'css/animate.css',
        'css/morris.css',
        'css/style.css',
        'css/all-themes.css',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext',
        'js/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css',
        
    ];
    public $js = [
        //'js/jquery.min.js',
        //'js/bootstrap-select.js',
        'js/bootstrap.js',
        'js/jquery.slimscroll.js',
        'js/waves.js',
        //  'js/raphael.min.js',
        // 'js/morris.js',
        // 'js/Chart.bundle.js',
        'js/index.js',
        'js/admin.js',
        'js/waves.js',
        //'js/jquery.countTo.js',
       
        // 'js/jquery.flot.js',
        // 'js/jquery.flot.resize.js',
        // 'js/jquery.flot.pie.js',
        // 'js/jquery.flot.categories.js',
        // 'js/jquery.flot.time.js',
        // 'js/jquery.sparkline.js',
        'js/forms/basic-form-elements.js',
        
       
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
