<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $pid
 * @property string $p_name
 * @property string $p_price
 * @property string $p_bid_price
 * @property int $p_soldto
 * @property string $p_start_time
 * @property string $p_bidcategory
 * @property string $p_lastbid
 * @property string $p_imgpath
 * @property string $p_addedon
 * @property int $p_isdelete
 *
 * @property UserDetails $pSoldto
 * @property BidCategory $pBidcategory
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p_name', 'p_price', 'p_bid_price',  'p_bidcategory'], 'required'],
            [['p_soldto', 'p_isdelete', 'p_bidcategory'], 'integer'],
            [['p_lastbid', 'p_addedon', 'p_addedon', 'p_isdelete','p_soldto','p_imgpath', 'p_start_time'], 'safe'],
            [['file'],'file'],
            [['p_name', 'p_price', 'p_bid_price','p_imgpath'], 'string', 'max' => 100],
            [['p_start_time'], 'string', 'max' => 50],
            [['p_soldto'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['p_soldto' => 'user_id']],
            [['p_bidcategory'], 'exist', 'skipOnError' => true, 'targetClass' => BidCategory::className(), 'targetAttribute' => ['p_bidcategory' => 'bidcat_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pid' => 'Pid',
            'p_name' => 'Product Name',
            'p_price' => 'Product Price',
            'p_bid_price' => 'Product Bid Price',
            'p_soldto' => 'Product Soldto',
            'p_start_time' => 'Product Start Time',
            'p_bidcategory' => 'Product Bidcategory',
            'file'=>'Product',
            'p_lastbid' => 'Product Lastbid',
            'p_imgpath' => 'Product Imgpath',
            'p_addedon' => 'Product Addedon',
            'p_isdelete' => 'Product Isdelete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPSoldto()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'p_soldto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPBidcategory()
    {
        return $this->hasOne(BidCategory::className(), ['bidcat_id' => 'p_bidcategory']);
    }
}
