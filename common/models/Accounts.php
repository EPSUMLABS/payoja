<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "accounts".
 *
 * @property int $acc_id
 * @property int $acc_user_id
 * @property string $acc_freebids
 * @property string $acc_paidbids
 * @property int $acc_spins
 * @property string $acc_credits
 * @property string $acc_lastspintime
 * @property string $acc_lastfreebid_time
 *
 * @property UserDetails $accUser
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['acc_user_id', 'acc_freebids', 'acc_paidbids', 'acc_spins', 'acc_credits'], 'required'],
            [['acc_user_id', 'acc_paidbids','acc_freebids',  'acc_spins', 'acc_credits'], 'integer'],
            [['acc_lastspintime','acc_lastfreebid_time'], 'safe'],
            [['acc_user_id'], 'unique'],
            [['acc_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['acc_user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'acc_id' => 'Acc ID',
            'acc_user_id' => 'Acc User ID',
            'acc_freebids' => 'Acc Freebids',
            'acc_paidbids' => 'Acc Pairbids',
            'acc_spins' => 'Acc Spins',
            'acc_credits' => 'Acc Credits',
            'acc_lastspintime' => 'Acc Lastspintime',
            'acc_lastfreebid_time' => 'Acc Lastfreebid Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccUser()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'acc_user_id']);
    }
}
