<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "admin".
 *
 * @property int $admin_id
 * @property string $admin_name
 * @property string $admin_email
 * @property string $admin_phone
 * @property string $admin_password
 * @property string $added_on
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_name', 'admin_email', 'admin_phone', 'admin_password', 'added_on'], 'required'],
            [['added_on'], 'safe'],
            [['admin_name', 'admin_email', 'admin_password'], 'string', 'max' => 100],
            [['admin_phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'admin_id' => 'Admin ID',
            'admin_name' => 'Admin Name',
            'admin_email' => 'Admin Email',
            'admin_phone' => 'Admin Phone',
            'admin_password' => 'Admin Password',
            'added_on' => 'Added On',
        ];
    }
}
