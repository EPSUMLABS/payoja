<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bid_category".
 *
 * @property int $bidcat_id
 * @property string $bidcat_name
 * @property int $bid_cat_days
 * @property int $bid_cat_hour
 * @property int $bid_cat_mins
 * @property int $bid_cat_seconds
 * @property string $bidcat_addedon
 *
 * @property Product[] $products
 */
class BidCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bid_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bidcat_name', 'bidcat_addedon'], 'required'],
            [['bid_cat_days', 'bid_cat_hour', 'bid_cat_mins', 'bid_cat_seconds'], 'integer'],
            [['bidcat_addedon'], 'safe'],
            [['bidcat_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bidcat_id' => 'Bidcategory ID',
            'bidcat_name' => 'Bid category Name',
            'bid_cat_days' => 'Bid category Days',
            'bid_cat_hour' => 'Bid category Hour',
            'bid_cat_mins' => 'Bid category Mins',
            'bid_cat_seconds' => 'Bid category Seconds',
            'bidcat_addedon' => 'Bid category Addedon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['p_bidcategory' => 'bidcat_id']);
    }
}
