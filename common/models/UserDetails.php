<?php

namespace common\models;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "user_details".
 *
 * @property int $user_id
 * @property string $u_name
 * @property string $u_email
 * @property string $u_phoneno
 * @property string $u_password
 * @property int $u_accountid
 * @property string $uverify_token
 * @property string $ufb_token
 * @property string $ufb_id
 * @property string $u_referalid
 * @property int $u_isconfirmed
 * @property int $is_delete
 * @property string $u_added
 *
 * @property Accounts[] $accounts
 * @property Product[] $products
 * @property Referals[] $referals
 * @property Referals[] $referals0
 * @property Transaction[] $transactions
 */
class UserDetails extends ActiveRecord 

{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['u_name', 'u_email', 'u_phoneno', 'u_password'], 'required'],
            [['u_accountid', 'u_isconfirmed', 'is_delete'], 'integer'],
            [['u_added', 'u_accountid', 'ufb_token', 'ufb_id', 'u_referalid', 'u_isconfirmed', 'is_delete', 'u_added'], 'safe'],
            [['u_name', 'u_email', 'u_password', 'ufb_token', 'ufb_id', 'u_referalid'], 'string', 'max' => 100],
            [['u_phoneno'], 'string', 'max' => 12],
            [['uverify_token'], 'string', 'max' => 50],
            [['u_email'], 'unique'],
            [['u_phoneno'], 'unique'],
            [['u_referalid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'u_name' => 'User Name',
            'u_email' => 'User Email',
            'u_phoneno' => 'User Phoneno',
            'u_password' => 'User Password',
            'u_accountid' => 'User Accountid',
            'uverify_token' => 'Uverify Token',
            'ufb_token' => 'Ufb Token',
            'ufb_id' => 'Ufb ID',
            'u_referalid' => 'User Referalid',
            'u_isconfirmed' => 'U Isconfirmed',
            'is_delete' => 'Is Delete',
            'u_added' => 'User Joined On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Accounts::className(), ['acc_user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['p_soldto' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferals()
    {
        return $this->hasMany(Referals::className(), ['referrer_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferals0()
    {
        return $this->hasMany(Referals::className(), ['referee_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['trans_user_id' => 'user_id']);
    }
}
