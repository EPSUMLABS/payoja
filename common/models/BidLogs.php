<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bid_logs".
 *
 * @property int $bid_log_id
 * @property int $buser_id
 * @property string $bid_value
 * @property int $bid_product_id
 * @property string $bidding_time
 *
 * @property UserDetails $buser
 * @property Product $bidProduct
 */
class BidLogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bid_logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['buser_id', 'bid_value', 'bid_product_id', 'bidding_time'], 'required'],
            [['buser_id', 'bid_product_id'], 'integer'],
            [['bid_value'], 'number'],
            [['bidding_time'], 'safe'],
            [['buser_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['buser_id' => 'user_id']],
            [['bid_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['bid_product_id' => 'pid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bid_log_id' => 'Bid Log ID',
            'buser_id' => 'Buser ID',
            'bid_value' => 'Bid Value',
            'bid_product_id' => 'Bid Product ID',
            'bidding_time' => 'Bidding Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuser()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'buser_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBidProduct()
    {
        return $this->hasOne(Product::className(), ['pid' => 'bid_product_id']);
    }
}
