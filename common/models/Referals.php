<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "referals".
 *
 * @property int $referral_id
 * @property int $referrer_id
 * @property int $referee_id
 * @property int $transaction_status
 * @property string $radded_on
 *
 * @property UserDetails $referrer
 * @property UserDetails $referee
 */
class Referals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'referals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['referrer_id', 'referee_id',  'radded_on'], 'required'],
            [['referrer_id', 'referee_id', 'transaction_status'], 'integer'],
            [['radded_on','transaction_status'], 'safe'],
            [['referee_id'], 'unique'],
            [['referrer_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['referrer_id' => 'user_id']],
            [['referee_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['referee_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'referral_id' => 'Referral ID',
            'referrer_id' => 'Referrer ID',
            'referee_id' => 'Referee ID',
            'transaction_status' => 'Transaction Status',
            'radded_on' => 'Radded On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferrer()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'referrer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferee()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'referee_id']);
    }
}
