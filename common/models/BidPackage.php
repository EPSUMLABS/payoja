<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bid_package".
 *
 * @property int $bidpack_id
 * @property string $bpk_name
 * @property int $bpk_bids
 * @property string $bpk_price
 * @property string $bpk_added_on
 * @property int $bpk_isdelete
 */
class BidPackage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bid_package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bpk_name', 'bpk_bids', 'bpk_price' ], 'required'],
            [['bpk_bids', 'bpk_isdelete'], 'integer'],
            [['bpk_added_on','bpk_added_on', 'bpk_isdelete'], 'safe'],
            [['bpk_name', 'bpk_price'], 'string', 'max' => 100],
            [['bpk_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bidpack_id' => 'Bidpack ID',
            'bpk_name' => 'Bid Package Name',
            'bpk_bids' => 'Bid Package Bids',
            'bpk_price' => 'Bid Package Price',
            'bpk_added_on' => 'Bpk Added On',
            'bpk_isdelete' => 'Bpk Isdelete',
        ];
    }
}
