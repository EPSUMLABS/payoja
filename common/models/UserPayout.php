<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_payout".
 *
 * @property int $payout_id
 * @property int $user_id
 * @property string $user_name
 * @property int $paytm_phoneno
 * @property string $user_bankacc_no
 * @property string $user_bankifsc_code
 * @property int $amount
 * @property string $transaction_id
 * @property string $status
 * @property string $date_of_request
 */
class UserPayout extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_payout';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'user_bankacc_no', 'user_bankifsc_code', 'amount'], 'required'],
            [['user_id', 'paytm_phoneno'], 'integer'],
            [['amount'], 'number'],
            [['date_of_request', 'transaction_id', 'status', 'date_of_request'], 'safe'],
            [['user_name', 'user_bankacc_no', 'user_bankifsc_code', 'transaction_id'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'payout_id' => 'Payout ID',
            'user_id' => 'User ID',
            'user_name' => 'User Name',
            'paytm_phoneno' => 'Paytm Phoneno',
            'user_bankacc_no' => 'User Bankacc No',
            'user_bankifsc_code' => 'User Bankifsc Code',
            'amount' => 'Amount',
            'transaction_id' => 'Transaction ID',
            'status' => 'Status',
            'date_of_request' => 'Date Of Request',
        ];
    }
}
